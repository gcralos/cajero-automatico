CREATE TABLE  IF NOT EXISTS accounts(
    id Varchar(200) not null,
    numeroCuenta VARCHAR(100) not null,
    nombreCuenta  Varchar(50) not null,
    saldo Decimal(10,2) not null
);
CREATE TABLE  IF NOT EXISTS transaciones (
    id int not null,
    transaccionID Varchar(200) not null,
    cuentaOrigen VARCHAR(100) not null,
    operacion VARCHAR(100) not null,
    monto Decimal(10,2) not null,
    cuentaDestino  Varchar(100) not null,
    fechaMovimiento Timestamp not null
);
