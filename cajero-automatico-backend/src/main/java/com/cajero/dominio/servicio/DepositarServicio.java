package com.cajero.dominio.servicio;

import com.cajero.commun.dominio.enums.Operacion;
import com.cajero.commun.dominio.exepctions.ExceptionNotAccount;
import com.cajero.commun.dominio.exepctions.ExceptionNotValueZero;
import com.cajero.dominio.modelo.entidad.Cuenta;
import com.cajero.dominio.modelo.entidad.Transaccion;
import com.cajero.dominio.puerto.repositorio.TipoOperacionRepositorio;
import com.cajero.dominio.puerto.repositorio.TransaccionRepositorio;

public class DepositarServicio {
    private  final TipoOperacionRepositorio tipoOperacionRepositorio;
    private final TransaccionRepositorio transactionRepositorio;
    private final String NO_APLICA = "NO_APLICA";
    private Double saldoTotal=0.0;
    public DepositarServicio(TipoOperacionRepositorio tipoOperacionRepositorio, TransaccionRepositorio transactionRepositorio) {
        this.tipoOperacionRepositorio = tipoOperacionRepositorio;
        this.transactionRepositorio = transactionRepositorio;
    }
    public void ejecutar(Cuenta cuenta, Double monto, Operacion operacion){
      if (validarExistenciaCuenta(cuenta))
          depositar(cuenta,monto,operacion);
    }
    private Boolean validarExistenciaCuenta(Cuenta cuenta) {
        Boolean  existeCuenta =tipoOperacionRepositorio.existe(cuenta.getNumeroCuenta());
        if(!existeCuenta)
            throw new ExceptionNotAccount();
        return true;
    }
    private void depositar(Cuenta cuenta, Double monto,Operacion operacion) {
        if(monto <= 0 )
            throw new ExceptionNotValueZero();
        saldoTotal= cuenta.getSaldo()+monto;
        cuenta.setSaldo(saldoTotal);
        tipoOperacionRepositorio.operacionDeposito(cuenta);
        if(!operacion.equals(Operacion.TRANFERENCIA))
            transactionRepositorio.transaccion(new Transaccion(NO_APLICA, Operacion.DEPOSITO.toString(),monto,cuenta.getNumeroCuenta()));

    }
}
