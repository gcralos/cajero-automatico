package com.cajero.dominio.servicio;

import com.cajero.commun.dominio.enums.Operacion;
import com.cajero.commun.dominio.exepctions.ExceptionErrorAgreement;
import com.cajero.dominio.modelo.entidad.Cuenta;
import com.cajero.dominio.modelo.entidad.Transaccion;
import com.cajero.dominio.puerto.repositorio.TransaccionRepositorio;

public class TransferirServicio {
    private final  DepositarServicio depositarServicio;
    private final  RetirarServicio  retirarServicio;
    private final TransaccionRepositorio transaccionRepositorio;

    public TransferirServicio(DepositarServicio depositarServicio, RetirarServicio retirarServicio, TransaccionRepositorio transaccionRepositorio) {
        this.depositarServicio = depositarServicio;
        this.retirarServicio = retirarServicio;
        this.transaccionRepositorio = transaccionRepositorio;
    }
    public void  ejecutar(Cuenta cuentaOrigen, Cuenta  cuentaDestino, Double monto, Operacion operacion){
         if(cuentaOrigen.equals(cuentaDestino))
             throw new ExceptionErrorAgreement();
        retirarServicio.ejecutar(cuentaOrigen,monto,operacion);
        depositarServicio.ejecutar(cuentaDestino,monto,operacion);
        transaccionRepositorio.transaccion(new Transaccion(cuentaOrigen.getNumeroCuenta(),Operacion.TRANFERENCIA.toString(),monto,cuentaDestino.getNumeroCuenta()));
    }



}
