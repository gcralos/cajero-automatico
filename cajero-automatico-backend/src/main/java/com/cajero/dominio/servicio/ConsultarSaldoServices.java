package com.cajero.dominio.servicio;

import com.cajero.commun.dominio.exepctions.ExceptionNotAccount;
import com.cajero.dominio.modelo.dto.CuentaDTO;
import com.cajero.dominio.puerto.repositorio.TipoOperacionRepositorio;

import java.util.List;

public class ConsultarSaldoServices {
    private  final TipoOperacionRepositorio tipoOperacionRepositorio;

    public ConsultarSaldoServices(TipoOperacionRepositorio tipoOperacionRepositorio) {
        this.tipoOperacionRepositorio = tipoOperacionRepositorio;
    }
    public List<CuentaDTO> ejecutar(String numeroCuenta){
        if(validarExistenciaCuenta(numeroCuenta)){
        return  consultarSaldo(numeroCuenta);}
        return  null;
    }

    private List<CuentaDTO> consultarSaldo(String numeroCuenta) {
        return tipoOperacionRepositorio.consultarSaldo(numeroCuenta);
    }

    private Boolean validarExistenciaCuenta(String  numeroCuenta) {
        Boolean  existeCuenta =tipoOperacionRepositorio.existe(numeroCuenta);
        if(!existeCuenta)
            throw new ExceptionNotAccount();
        return true;
    }
}
