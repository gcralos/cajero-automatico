package com.cajero.dominio.servicio;

import com.cajero.commun.dominio.enums.Operacion;
import com.cajero.commun.dominio.exepctions.ExceptionNotAccount;
import com.cajero.commun.dominio.exepctions.ExceptionTransattionError;
import com.cajero.dominio.modelo.entidad.Cuenta;
import com.cajero.dominio.modelo.entidad.Transaccion;
import com.cajero.dominio.puerto.repositorio.TipoOperacionRepositorio;
import com.cajero.dominio.puerto.repositorio.TransaccionRepositorio;

public class RetirarServicio {
    private final TipoOperacionRepositorio  tipoOperacionRepositorio;
    private final TransaccionRepositorio transactionRepositorio;
    private final String NO_APLICA = "NO_APLICA";
    private Double saldoTotal=0.0;
    public RetirarServicio(TipoOperacionRepositorio tipoOperacionRepositorio, TransaccionRepositorio transactionRepositorio) {
        this.tipoOperacionRepositorio = tipoOperacionRepositorio;
        this.transactionRepositorio = transactionRepositorio;
    }
    public void  ejecutar(Cuenta cuenta,Double monto,Operacion operacion){
        if(validarExistenciaCuenta(cuenta))
            retirar(cuenta,monto,operacion);
    }
    private Boolean validarExistenciaCuenta(Cuenta cuenta) {
        Boolean  existeCuenta =tipoOperacionRepositorio.existe(cuenta.getNumeroCuenta());
        if(!existeCuenta)
            throw new ExceptionNotAccount();
        return existeCuenta;
    }
    private void retirar(Cuenta cuenta, Double monto,Operacion operacion) {
        if((monto<= 0) || (monto > cuenta.getSaldo()))
            throw new ExceptionTransattionError();
        saldoTotal= cuenta.getSaldo()-monto;
        cuenta.setSaldo(saldoTotal);
        tipoOperacionRepositorio.operacionRetiro(cuenta);
        if (!operacion.equals(Operacion.TRANFERENCIA))
            transactionRepositorio.transaccion(new Transaccion(cuenta.getNumeroCuenta(), Operacion.RETIRO.toString(),monto,NO_APLICA));
    }
}
