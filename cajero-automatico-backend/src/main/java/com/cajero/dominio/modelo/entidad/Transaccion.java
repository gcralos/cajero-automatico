package com.cajero.dominio.modelo.entidad;

import com.cajero.commun.dominio.enums.Operacion;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import static com.cajero.commun.dominio.validacion.ValidarArgumentos.*;

@Getter
@Setter
public class Transaccion {
    private Long id;
    private String  transaccionID;
    private String  cuentaOrigen;
    private String operacion;
    private Double  monto;
    private String cuentaDestino;
    private LocalDateTime fechaMovimiento;

    public Transaccion( String numeroCuenta, String operacion, Double monto,String cuentaDestino) {
        validarCampoObligatorio(numeroCuenta);
        validarCampoObligatorio(operacion);
        validarCampoObligatorio(cuentaDestino);
        validarCampoObligatorio(monto);
        validarCampoMayorCero(monto);
        this.id=1L;
        this.transaccionID = java.util.UUID.randomUUID().toString();
        this.cuentaOrigen =  numeroCuenta;
        this.operacion = operacion;
        this.monto = monto;
        this.cuentaDestino = cuentaDestino;
        this.fechaMovimiento = LocalDateTime.now();
    }

}
