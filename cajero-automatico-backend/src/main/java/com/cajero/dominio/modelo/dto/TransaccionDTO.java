package com.cajero.dominio.modelo.dto;

import com.cajero.commun.dominio.enums.Operacion;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransaccionDTO {
    private Long  id;
    private String  transaccionID;
    private String cuentaOrigen ;
    private String operacion;
    private Double  monto;
    private String cuentaDestino;
    private LocalDateTime  fechaMovimiento;
}
