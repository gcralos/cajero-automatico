package com.cajero.dominio.modelo.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CuentaDTO {
    private String  id;
    private String  numeroCuenta;
    private String nombreCuenta;
    private Double saldo;

}
