package com.cajero.dominio.modelo.entidad;

import lombok.Getter;
import lombok.Setter;

import static com.cajero.commun.dominio.validacion.ValidarArgumentos.validarCampoObligatorio;
import static com.cajero.commun.dominio.validacion.ValidarArgumentos.validarCampoSoloCarateres;
@Getter
@Setter
public class Cuenta {
    private String  id;
    private String  numeroCuenta;
    private String nombreCuenta;
    private Double saldo;

    public Cuenta() {
    }

    public Cuenta(String numeroCuenta, String nombreCuenta, Double saldo) {
        validarCampoObligatorio(numeroCuenta);
        validarCampoObligatorio(nombreCuenta);
        validarCampoSoloCarateres(nombreCuenta);
        this.numeroCuenta = numeroCuenta;
        this.nombreCuenta = nombreCuenta;
        this.saldo = saldo;
    }
    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
}
