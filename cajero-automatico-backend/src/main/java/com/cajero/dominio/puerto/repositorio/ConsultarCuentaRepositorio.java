package com.cajero.dominio.puerto.repositorio;

import com.cajero.dominio.modelo.dto.CuentaDTO;

public interface ConsultarCuentaRepositorio {
    CuentaDTO  getCuentaDTO();
}
