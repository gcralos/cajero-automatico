package com.cajero.dominio.puerto.repositorio;

import com.cajero.dominio.modelo.dto.TransaccionDTO;
import com.cajero.dominio.modelo.entidad.Transaccion;

import java.util.List;

public interface TransaccionRepositorio {
    public Long  transaccion(Transaccion cuentaOrigen);
}
