package com.cajero.dominio.puerto.dao;

import com.cajero.dominio.modelo.dto.CuentaDTO;
import org.springframework.stereotype.Repository;

import java.util.List;
public interface ConsultarCuentaDAO {
    List<CuentaDTO> consultarCuentas();
}
