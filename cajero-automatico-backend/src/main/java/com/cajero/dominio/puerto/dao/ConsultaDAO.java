package com.cajero.dominio.puerto.dao;

import com.cajero.dominio.modelo.dto.TransaccionDTO;
import org.springframework.stereotype.Component;

import java.util.List;
public interface ConsultaDAO {
    List<TransaccionDTO> transaccionesList();
    List<TransaccionDTO> transaccionesList(String numeroCuenta);
}
