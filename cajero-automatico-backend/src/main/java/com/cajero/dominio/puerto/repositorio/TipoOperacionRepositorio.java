package com.cajero.dominio.puerto.repositorio;

import com.cajero.dominio.modelo.dto.CuentaDTO;
import com.cajero.dominio.modelo.entidad.Cuenta;

import java.util.List;

public interface TipoOperacionRepositorio {
    public void operacionDeposito(Cuenta cuenta);

    public void operacionRetiro(Cuenta cuenta);

    public Boolean existe(String numeroCuenta);

    public List<CuentaDTO> consultarSaldo(String  numeroCuenta);
}

