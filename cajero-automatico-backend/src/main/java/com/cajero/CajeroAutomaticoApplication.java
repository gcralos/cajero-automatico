package com.cajero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.cajero.*"})
public class CajeroAutomaticoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CajeroAutomaticoApplication.class, args);
    }

}
