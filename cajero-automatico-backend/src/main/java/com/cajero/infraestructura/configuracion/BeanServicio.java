package com.cajero.infraestructura.configuracion;

import com.cajero.dominio.puerto.repositorio.TipoOperacionRepositorio;

import com.cajero.dominio.puerto.repositorio.TransaccionRepositorio;
import com.cajero.dominio.servicio.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanServicio {

    @Bean
    public ConsultarSaldoServices consultarSaldoServices( TipoOperacionRepositorio tipoOperacionRepositorio){
        return new ConsultarSaldoServices(tipoOperacionRepositorio);
    }
    @Bean
    public DepositarServicio depositarServicio(TipoOperacionRepositorio tipoOperacionRepositorio, TransaccionRepositorio transaccionRepositorio){
        return  new DepositarServicio(tipoOperacionRepositorio,transaccionRepositorio);
    }

    @Bean
    public RetirarServicio  retirarServicio(TipoOperacionRepositorio tipoOperacionRepositorio, TransaccionRepositorio transaccionRepositorio){
        return  new RetirarServicio(tipoOperacionRepositorio,transaccionRepositorio);
    }
    @Bean
    public TransferirServicio  transferirServicio(DepositarServicio depositarServicio,RetirarServicio retirarServicio,TransaccionRepositorio transaccionRepositorio){
        return  new TransferirServicio(depositarServicio,retirarServicio, transaccionRepositorio);
    }

}
