package com.cajero.infraestructura.adaptador.dao;

import com.cajero.commun.infraestructura.jdbc.CustomNamedParameterJdbcTemplate;
import com.cajero.commun.infraestructura.jdbc.sqlstatement.SqlStatement;
import com.cajero.dominio.modelo.dto.CuentaDTO;
import com.cajero.dominio.modelo.dto.TransaccionDTO;
import com.cajero.dominio.puerto.dao.ConsultaDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@RequiredArgsConstructor
public class TransacionesDAOH2  implements ConsultaDAO {
    private  final CustomNamedParameterJdbcTemplate customNamedParameterJdbcTemplate;

    @SqlStatement(namespace = "cuenta", value = "ConsultarTodasLastransaciones")
    private static String  consultarTodasLastransaciones;
    @SqlStatement(namespace = "cuenta", value = "TransaccionHistoricoCuenta")
    private static String  transaccionHistoricoCuenta;


    @Override
    public List<TransaccionDTO> transaccionesList() {
        return this.customNamedParameterJdbcTemplate.getNamedParameterJdbcTemplate()
                .query(consultarTodasLastransaciones, BeanPropertyRowMapper.newInstance(TransaccionDTO.class));
    }

    @Override
    public List<TransaccionDTO> transaccionesList(String numeroCuenta) {
        MapSqlParameterSource mapSqlParameterSource  = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("numeroCuenta", numeroCuenta);
        return this.customNamedParameterJdbcTemplate.getNamedParameterJdbcTemplate()
                .query(transaccionHistoricoCuenta, mapSqlParameterSource, BeanPropertyRowMapper.newInstance(TransaccionDTO.class));
    }
}
