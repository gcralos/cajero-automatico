package com.cajero.infraestructura.adaptador.repositorio;

import com.cajero.commun.infraestructura.jdbc.CustomNamedParameterJdbcTemplate;
import com.cajero.commun.infraestructura.jdbc.sqlstatement.SqlStatement;
import com.cajero.dominio.modelo.dto.CuentaDTO;
import com.cajero.dominio.modelo.dto.TransaccionDTO;
import com.cajero.dominio.modelo.entidad.Cuenta;
import com.cajero.dominio.modelo.entidad.Transaccion;
import com.cajero.dominio.puerto.repositorio.TipoOperacionRepositorio;
import com.cajero.dominio.puerto.repositorio.TransaccionRepositorio;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@AllArgsConstructor
public class OperacionesCuentasRespositositorio  implements TipoOperacionRepositorio, TransaccionRepositorio {

    private final CustomNamedParameterJdbcTemplate customNamedParameterJdbcTemplate;

    @SqlStatement(namespace = "operaciones", value="ConsultarNumeroCuenta")
    private static  String consultarNumeroCuenta;

    @SqlStatement(namespace = "cuenta", value="ConsultarCuenta")
    private static  String consultarcuenta;

    @SqlStatement(namespace = "operaciones", value="OperacionCuenta")
    private static  String operacionCuenta;

    @SqlStatement(namespace = "cuenta", value="InsertTransacionHistorico")
    private static  String insertTransacionHistorico;
    @SqlStatement(namespace = "cuenta", value="TransaccionHistoricoCuenta")
    private static  String transaccionHistoricoCuenta;

    @Override
    public void operacionDeposito(Cuenta cuenta) {
        this.customNamedParameterJdbcTemplate.actualizar(cuenta,operacionCuenta);
    }

    @Override
    public void operacionRetiro(Cuenta cuenta) {
        this.customNamedParameterJdbcTemplate.actualizar(cuenta,operacionCuenta);
    }
    @Override
    public Boolean existe(String numeroCuenta) {
        MapSqlParameterSource mapSqlParameterSource = getMapSqlParameterSource(numeroCuenta);
        return this.customNamedParameterJdbcTemplate.getNamedParameterJdbcTemplate()
                .queryForObject(consultarNumeroCuenta,mapSqlParameterSource, Boolean.class);
    }
    @Override
    public List<CuentaDTO> consultarSaldo(String numeroCuenta) {
        MapSqlParameterSource mapSqlParameterSource = getMapSqlParameterSource(numeroCuenta);
        return this.customNamedParameterJdbcTemplate
                .getNamedParameterJdbcTemplate()
                .query(consultarcuenta,mapSqlParameterSource, BeanPropertyRowMapper.newInstance(CuentaDTO.class));
    }

    @Override
    public Long transaccion(Transaccion cuentaOrigen) {
        return this.customNamedParameterJdbcTemplate.crear(cuentaOrigen,insertTransacionHistorico);
    }

    private MapSqlParameterSource getMapSqlParameterSource(String numeroCuenta) {
        MapSqlParameterSource  mapSqlParameterSource  = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("numeroCuenta", numeroCuenta);
        return mapSqlParameterSource;
    }
}
