package com.cajero.infraestructura.adaptador.dao;

import com.cajero.commun.infraestructura.jdbc.CustomNamedParameterJdbcTemplate;
import com.cajero.commun.infraestructura.jdbc.sqlstatement.SqlStatement;
import com.cajero.dominio.modelo.dto.CuentaDTO;
import com.cajero.dominio.puerto.dao.ConsultarCuentaDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@RequiredArgsConstructor
public class ConsultarCuentaDAOH2 implements ConsultarCuentaDAO {
    private  final CustomNamedParameterJdbcTemplate  customNamedParameterJdbcTemplate;

    @SqlStatement(namespace = "cuenta", value = "ConsultarTodasLasCuentas")
    private static String  consultarTodasCuentas;
    @Override
    public List<CuentaDTO> consultarCuentas() {
        return this.customNamedParameterJdbcTemplate.getNamedParameterJdbcTemplate()
                .query(consultarTodasCuentas, BeanPropertyRowMapper.newInstance(CuentaDTO.class));
    }
}
