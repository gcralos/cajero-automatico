package com.cajero.infraestructura.controlador;


import com.cajero.commun.dominio.enums.Operacion;
import com.cajero.dominio.modelo.dto.CuentaDTO;
import com.cajero.dominio.modelo.dto.TransaccionDTO;
import com.cajero.dominio.modelo.entidad.Cuenta;
import com.cajero.dominio.servicio.*;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class OperacionesCuentaController {
     private final DepositarServicio depositarServicio;
     private final RetirarServicio  retirarServicio;
     private final ConsultarSaldoServices consultaSaldo;
     private final TransferirServicio  transferirServicio;

     @RequestMapping(value="/depositar-saldo/{numerocuenta}/{monto}",method = RequestMethod.PUT)
     public  void   operacionDepositar(@PathVariable String numerocuenta,@PathVariable Double monto){
          Cuenta cuenta = getCuenta(this.consultaSaldo.ejecutar(numerocuenta).get(0));
          depositarServicio.ejecutar(cuenta,monto, Operacion.DEPOSITO);
     }
     @RequestMapping(value="/retirar-saldo/{numerocuenta}/{monto}",method = RequestMethod.PUT)
     public  void   operacionRetirarSaldo(@PathVariable String numerocuenta,@PathVariable Double monto){
          Cuenta cuenta = getCuenta(this.consultaSaldo.ejecutar(numerocuenta).get(0));
          retirarServicio.ejecutar(cuenta,monto,Operacion.RETIRO);
     }
     @RequestMapping(value="/consulta-saldo/{numerocuenta}",method = RequestMethod.GET)
     public List<CuentaDTO> consultaSaldoCuenta(@PathVariable String numerocuenta){
          return consultaSaldo.ejecutar(numerocuenta);
     }
     @RequestMapping(value="/transferencia-saldo/{cuentaOrigen}/{cuentaDestino}/{monto}",method = RequestMethod.POST)
     public void  transferenciaSaldo(@PathVariable String cuentaOrigen, @PathVariable String cuentaDestino,@PathVariable Double monto){
          Cuenta cuentaOrigens = getCuenta(this.consultaSaldo.ejecutar(cuentaOrigen).get(0));
          Cuenta cuentaDestinos = getCuenta(this.consultaSaldo.ejecutar(cuentaDestino).get(0));
          transferirServicio.ejecutar(cuentaOrigens,cuentaDestinos,monto,Operacion.TRANFERENCIA);
     }

     private Cuenta getCuenta(CuentaDTO cuentaDTO) {
          ModelMapper modelMapper = new ModelMapper();
          Cuenta  cuenta = modelMapper.map(cuentaDTO,Cuenta.class);
          return cuenta;
     }
}
