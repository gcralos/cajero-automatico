package com.cajero.infraestructura.controlador;

import com.cajero.dominio.modelo.dto.TransaccionDTO;
import com.cajero.infraestructura.adaptador.dao.TransacionesDAOH2;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TransacionesConsultaController {
    private final TransacionesDAOH2 transacionesDAOH2;

    public TransacionesConsultaController(TransacionesDAOH2 transacionesDAOH2) {
        this.transacionesDAOH2 = transacionesDAOH2;
    }
    @GetMapping("transaciones-cuentas")
    @ResponseBody
    public List<TransaccionDTO> consultarTransacciones(){
        return transacionesDAOH2.transaccionesList();
    }

    @GetMapping("transaciones-cuenta/{numeroCuenta}")
    @ResponseBody
    public List<TransaccionDTO> consultarTransaccionesID(@PathVariable String numeroCuenta){
        return transacionesDAOH2.transaccionesList(numeroCuenta);
    }
}
