package com.cajero.infraestructura.controlador;

import com.cajero.dominio.modelo.dto.CuentaDTO;
import com.cajero.dominio.modelo.dto.TransaccionDTO;
import com.cajero.infraestructura.adaptador.dao.ConsultarCuentaDAOH2;
import com.cajero.infraestructura.adaptador.dao.TransacionesDAOH2;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CuentasConsultaController {
    private  final ConsultarCuentaDAOH2 consultarCuenta;
    private final TransacionesDAOH2 transacionesDAOH2;
    public CuentasConsultaController(ConsultarCuentaDAOH2 consultarCuenta, TransacionesDAOH2 transacionesDAOH2) {
        this.consultarCuenta = consultarCuenta;
        this.transacionesDAOH2 = transacionesDAOH2;
    }

    @GetMapping("cajero/cuentas")
    @ResponseBody
    public List<CuentaDTO> consultarCuenta(){
        return consultarCuenta.consultarCuentas();
    }

}
