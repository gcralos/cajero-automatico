package com.cajero.commun.dominio.exepctions;

public class ExceptionValueNotString extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static final String MENSAJE_ERROR = "Solo se permite texto";
    public ExceptionValueNotString() {
        super(MENSAJE_ERROR);
    }
}
