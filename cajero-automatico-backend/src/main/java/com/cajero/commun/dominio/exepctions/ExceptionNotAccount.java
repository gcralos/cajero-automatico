package com.cajero.commun.dominio.exepctions;

public class ExceptionNotAccount extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static final String MENSAJE_ERROR = "El numero  de cuenta no existe";
    public ExceptionNotAccount() {
        super(MENSAJE_ERROR);
    }
}
