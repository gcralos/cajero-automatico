package com.cajero.commun.dominio.exepctions;

public class ExceptionTransattionError extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static final String MENSAJE_ERROR = "Error al retirar saldo insuficiente";
    public ExceptionTransattionError() {
        super(MENSAJE_ERROR);
    }
}
