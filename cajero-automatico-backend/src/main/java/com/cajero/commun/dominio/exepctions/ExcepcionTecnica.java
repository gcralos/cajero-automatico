package com.cajero.commun.dominio.exepctions;

public class ExcepcionTecnica extends RuntimeException{
    private static final long serialVersionUID = 1L;
    private static final String MENSAJE_ERROR = "No tiene saldo para realizar  la tranzacion";
    public ExcepcionTecnica(Exception e) {
        super(MENSAJE_ERROR,e);
    }
}
