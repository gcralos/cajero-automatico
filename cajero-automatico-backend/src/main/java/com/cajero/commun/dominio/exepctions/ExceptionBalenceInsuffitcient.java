package com.cajero.commun.dominio.exepctions;

public class ExceptionBalenceInsuffitcient extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static final String MENSAJE_ERROR = "No tiene saldo para realizar  la tranzacion";
    public ExceptionBalenceInsuffitcient() {
        super(MENSAJE_ERROR);
    }
}
