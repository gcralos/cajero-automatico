package com.cajero.commun.dominio.exepctions;

public class ExceptionNotValueZero extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static final String MENSAJE_ERROR = "El valor no puede ser cero";
    public ExceptionNotValueZero() {
        super(MENSAJE_ERROR);
    }
}
