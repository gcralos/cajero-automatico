package com.cajero.commun.dominio.validacion;

import com.cajero.commun.dominio.exepctions.ExceptionNotNull;
import com.cajero.commun.dominio.exepctions.ExceptionNotValueZero;
import com.cajero.commun.dominio.exepctions.ExceptionValueNotString;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidarArgumentos {

 public static void  validarCampoObligatorio(Object object){
     if(object == null)
         throw  new ExceptionNotNull();
 }
 public static void  validarCampoMayorCero(Double valor){
     if(valor <= 0)
         throw new ExceptionNotValueZero();
 }
 public static void  validarCampoSoloCarateres(String valor){
     String regex  = "[a-zA-Z_0-9]";
     Pattern pattern = Pattern.compile(regex);
     Matcher matcher = pattern.matcher(valor);
     if(matcher.matches())
         throw  new ExceptionValueNotString();
 }

}
