package com.cajero.commun.dominio.exepctions;

public class ExceptionNotNull extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static final String MENSAJE_ERROR = "El valor es obligatorio";
    public ExceptionNotNull() {
        super(MENSAJE_ERROR);
    }
}
