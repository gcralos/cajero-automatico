package com.cajero.dominio.servicio;

import com.cajero.dominio.databuilder.CuentaTestDataBuilder;
import com.cajero.dominio.modelo.entidad.Cuenta;
import com.cajero.dominio.puerto.repositorio.TipoOperacionRepositorio;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

class ConsultarSaldoServicesTest {
    private TipoOperacionRepositorio tipoOperacionRepositorio;
    private List<Cuenta> cuenta;

    @BeforeEach
    public void setUp(){
        cuenta = new ArrayList<Cuenta>();
        cuenta.add(0,new CuentaTestDataBuilder().conNumeroCuenta("99999999'").build());
        tipoOperacionRepositorio = Mockito.mock(TipoOperacionRepositorio.class);
        Mockito.when(tipoOperacionRepositorio.existe(cuenta.get(0).getNumeroCuenta())).thenReturn(true);
    }

    @Test
    @DisplayName("Should check the balance in the account")
    public  void  checkBalanceAccount(){
        Mockito.when(tipoOperacionRepositorio.existe(Mockito.anyString())).thenReturn(true);
        ConsultarSaldoServices consultarSaldo = new ConsultarSaldoServices(tipoOperacionRepositorio);
        consultarSaldo.ejecutar(cuenta.get(0).getNumeroCuenta());
       // Assertions.assertEquals(c,consultarSaldo.ejecutar(cuenta.get(0).getNumeroCuenta()));
    }

    @Test
    @DisplayName("When does not  exist the account")
    public void  checkAccountIsExists(){
        Assertions.assertNotEquals(cuenta.get(0).getNumeroCuenta(),0);
    }

}