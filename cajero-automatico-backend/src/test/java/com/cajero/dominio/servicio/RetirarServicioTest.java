package com.cajero.dominio.servicio;

import com.cajero.commun.dominio.enums.Operacion;
import com.cajero.commun.dominio.exepctions.ExceptionTransattionError;
import com.cajero.dominio.databuilder.CuentaTestDataBuilder;
import com.cajero.dominio.databuilder.TrasaccionesTestDataBuilder;
import com.cajero.dominio.modelo.entidad.Cuenta;
import com.cajero.dominio.modelo.entidad.Transaccion;
import com.cajero.dominio.puerto.repositorio.TipoOperacionRepositorio;
import com.cajero.dominio.puerto.repositorio.TransaccionRepositorio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class RetirarServicioTest {

    private TipoOperacionRepositorio tipoOperacionRepositorio;
    private TransaccionRepositorio transaccionRepositorio;
    private DepositarServicio depositarServicio;
    private Cuenta cuenta;
    private Transaccion transaccion;
    private RetirarServicio retirarServicio;

    @BeforeEach
    public  void  seUpt(){
        cuenta  =  new CuentaTestDataBuilder().conNumeroCuenta("900000").build();
        transaccion = new TrasaccionesTestDataBuilder().build();
        tipoOperacionRepositorio = Mockito.mock(TipoOperacionRepositorio.class);
        transaccionRepositorio = Mockito.mock(TransaccionRepositorio.class);
        Mockito.when(tipoOperacionRepositorio.existe(Mockito.anyString())).thenReturn(true);
        Mockito.when(transaccionRepositorio.transaccion(transaccion)).thenReturn(1L);
        depositarServicio = new DepositarServicio(tipoOperacionRepositorio,transaccionRepositorio);
        depositarServicio.ejecutar(cuenta,100.0, Operacion.DEPOSITO);
        retirarServicio = new RetirarServicio(tipoOperacionRepositorio, transaccionRepositorio);
    }
    @Test
    @DisplayName("When the user remove balance the account")
    public  void checkRemoveBalanceAccount(){
        retirarServicio.ejecutar(cuenta,50.0,Operacion.RETIRO);
        Assertions.assertEquals(cuenta.getSaldo(),50.0);
    }
    @Test
    @DisplayName("When the balance remove is higher tha balance present")
    public  void  checkRemoveBalanceAccountHigherBalnacePresent(){
         ExceptionTransattionError thrown = Assertions.assertThrows( ExceptionTransattionError.class,()->{
             retirarServicio.ejecutar(cuenta,101.0,Operacion.RETIRO);
         },"Error al retirar saldo insuficiente");
         Assertions.assertEquals("Error al retirar saldo insuficiente",thrown.getMessage());
    }

    @Test
    @DisplayName("When the balance remove is higher tha balance present")
    public  void  checkRemoveBalanceAccountEqualZero(){
        ExceptionTransattionError thrown = Assertions.assertThrows( ExceptionTransattionError.class,()->{
            retirarServicio.ejecutar(cuenta,0.0,Operacion.RETIRO);
        },"Error al retirar saldo insuficiente");
        Assertions.assertEquals("Error al retirar saldo insuficiente",thrown.getMessage());
    }
}