package com.cajero.dominio.servicio;

import com.cajero.commun.dominio.enums.Operacion;
import com.cajero.commun.dominio.exepctions.ExceptionNotValueZero;
import com.cajero.dominio.databuilder.CuentaTestDataBuilder;
import com.cajero.dominio.databuilder.TrasaccionesTestDataBuilder;
import com.cajero.dominio.modelo.entidad.Cuenta;
import com.cajero.dominio.modelo.entidad.Transaccion;
import com.cajero.dominio.puerto.repositorio.TipoOperacionRepositorio;
import com.cajero.dominio.puerto.repositorio.TransaccionRepositorio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class DepositarServicioTest {
    private TipoOperacionRepositorio tipoOperacionRepositorio;
    private TransaccionRepositorio transaccionRepositorio;
    private DepositarServicio depositarServicio;
    private Cuenta cuenta;
    private Transaccion transaccion;

    @BeforeEach
    public  void  seUpt(){
        cuenta  =  new CuentaTestDataBuilder().conNumeroCuenta("900000").build();
        transaccion = new TrasaccionesTestDataBuilder().build();
        tipoOperacionRepositorio = Mockito.mock(TipoOperacionRepositorio.class);
        transaccionRepositorio = Mockito.mock(TransaccionRepositorio.class);
        Mockito.when(tipoOperacionRepositorio.existe(Mockito.anyString())).thenReturn(true);
        Mockito.when(transaccionRepositorio.transaccion(transaccion)).thenReturn(1L);
        depositarServicio = new DepositarServicio(tipoOperacionRepositorio,transaccionRepositorio);
    }

    @Test
    @DisplayName("When the user add  balance  in the account")
    public void checkAddBalanceAccount(){
        depositarServicio.ejecutar(cuenta,100.0, Operacion.DEPOSITO);
        Assertions.assertEquals(cuenta.getSaldo(),100.0);
    }
    @Test
    @DisplayName("When the balance is less  that zero")
    public void checkAddBalanceAccountLessZero(){
       ExceptionNotValueZero thrown = Assertions.assertThrows(ExceptionNotValueZero.class,()->{
            depositarServicio.ejecutar(cuenta,-100.0,Operacion.DEPOSITO);
        },"El valor no puede ser cero");
       Assertions.assertEquals("El valor no puede ser cero",thrown.getMessage());
    }


}