package com.cajero.dominio.databuilder;

import com.cajero.dominio.modelo.entidad.Cuenta;

public class CuentaTestDataBuilder {

    private String  id;
    private String  numeroCuenta;
    private String nombreCuenta;
    private Double saldo;

    public CuentaTestDataBuilder() {
        this.id = java.util.UUID.randomUUID().toString();
        this.nombreCuenta = "DaffyDuck";
        this.saldo = 0.0;
    }
    public  CuentaTestDataBuilder  conId(String id){
        this.id = id;
        return this;
    }
    public  CuentaTestDataBuilder  conNumeroCuenta(String numeroCuenta){
        this.numeroCuenta= numeroCuenta;
        return this;
    }
    public  CuentaTestDataBuilder  conNombreCuenta(String nombreCuenta){
        this.nombreCuenta = nombreCuenta;
        return this;
    }
    public  CuentaTestDataBuilder  conSaldo(Double saldo){
        this.saldo = saldo;
        return this;
    }

    public Cuenta build(){
        return  new Cuenta(numeroCuenta,nombreCuenta,saldo);
    }
}
