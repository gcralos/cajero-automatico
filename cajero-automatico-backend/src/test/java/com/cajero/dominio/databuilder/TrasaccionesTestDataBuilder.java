package com.cajero.dominio.databuilder;

import com.cajero.commun.dominio.enums.Operacion;
import com.cajero.dominio.modelo.entidad.Transaccion;

public class TrasaccionesTestDataBuilder {
    private String  numeroCuenta;
    private String operacion;
    private Double  monto;
    private String cuentaDestino;

    public TrasaccionesTestDataBuilder() {
        this.numeroCuenta = "80000";
        this.operacion= Operacion.DEPOSITO.toString();
        this.monto=100.0;
        this.cuentaDestino="70000";
    }
    public TrasaccionesTestDataBuilder conNumeroCuenta(String  numeroCuenta){
         this.numeroCuenta=numeroCuenta;
         return this;
    }
    public TrasaccionesTestDataBuilder conOperacion(String  operacion){
        this.operacion=operacion;
        return this;
    }
    public TrasaccionesTestDataBuilder conMonto(Double  monto){
        this.monto=monto;
        return this;
    }
    public TrasaccionesTestDataBuilder conCuentaDestino(String  cuentaDestino){
        this.cuentaDestino=cuentaDestino;
        return this;
    }
    public Transaccion build(){
        return new Transaccion(numeroCuenta,operacion,monto,cuentaDestino);
    }
}
