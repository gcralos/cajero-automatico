export  interface cuentaModel{
  id: string
  numeroCuenta: string
  nombreCuenta: string
  saldo: number
}
