export  interface  TransaccionModelo{
  id: number,
  transaccionID: string,
  cuentaOrigen: string
  operacion: string,
  monto: number,
  cuentaDestino: string,
  fechaMovimiento: string
}
