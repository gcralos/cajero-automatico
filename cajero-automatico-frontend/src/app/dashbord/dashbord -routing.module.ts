import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListaCuentasComponent} from "./lista-cuentas/lista-cuentas.component";
import {OperacionesComponent} from "./operaciones/operaciones.component";
import { TransaccionesComponent } from './transacciones/transacciones.component';

const routes: Routes = [
  { path:'',children: [
    {path:'', component:ListaCuentasComponent},
    {path:'operaciones', component:OperacionesComponent},
    {path:'transacciones', component:TransaccionesComponent}
  ]}];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class DashbordRoutingModule { }
