import { Component, OnInit } from '@angular/core';
import {CajeroAutomaticoService} from "../../services/cajero-automatico.service";
import {ActivatedRoute} from "@angular/router";


@Component({
  selector: 'app-lista-cuentas',
  templateUrl: './lista-cuentas.component.html',
  styleUrls: ['./lista-cuentas.component.scss']
})
export class ListaCuentasComponent implements OnInit {
  cuentas:any=[];

  constructor(private cajeroAutomaticoService: CajeroAutomaticoService,private route: ActivatedRoute) {
  }

  ngOnInit(): void {
  this.getCuentas();
  }
  getCuentas():void{
    this.cajeroAutomaticoService.listarCuentas().subscribe(result => {
      this.cuentas = result;
    });
  }
}
