import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {CajeroAutomaticoService} from "../../services/cajero-automatico.service";

@Component({
  selector: 'app-transacciones',
  templateUrl: './transacciones.component.html',
  styleUrls: ['./transacciones.component.scss']
})
export class TransaccionesComponent implements OnInit {
  transacciones:any=[];
  filtroCuenta: any;
  constructor(private cajeroAutomaticoService: CajeroAutomaticoService) {
    this.filtroCuenta= new FormGroup({
      filtroCuentas: new FormControl(),
    });
  }

  ngOnInit(): void {

    this.getTransaccioonesHistorico();
  }

  getTransaccioonesHistorico():void{
     this.cajeroAutomaticoService.listarHistoricosTranferencciasCuenta().subscribe(res =>{
        this.transacciones = res;
     })
  }
  getTransaccionesHistoricosCuentas(numeroCuenta:string):void{
    this.cajeroAutomaticoService.listarHistoricosTranferencciasCuentas(numeroCuenta).subscribe(res =>{
      this.transacciones = res;
    })
  }


  dataChanged($event: any) {
    if($event != "ALL"){
      this.getTransaccionesHistoricosCuentas($event);
    }else{
      this.getTransaccioonesHistorico();
    }
  }
}
