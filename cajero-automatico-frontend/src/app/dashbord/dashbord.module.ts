import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaCuentasComponent } from './lista-cuentas/lista-cuentas.component';
import { DashboardComponent } from './dashboard.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzCardModule } from 'ng-zorro-antd/card';
import {DashbordRoutingModule} from "./dashbord -routing.module";
import { RouterModule } from '@angular/router';
import { OperacionesComponent } from './operaciones/operaciones.component';
import { TransaccionesComponent } from './transacciones/transacciones.component';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputModule } from 'ng-zorro-antd/input';
import {ReactiveFormsModule} from "@angular/forms";
import { NzSpinModule } from 'ng-zorro-antd/spin';
@NgModule({
  declarations: [
    ListaCuentasComponent,
    DashboardComponent,
    OperacionesComponent,
    TransaccionesComponent
  ],
  imports: [
    CommonModule,
    NzButtonModule,
    NzTableModule,
    NzCardModule,
    DashbordRoutingModule,
    RouterModule,
    NzStepsModule,
    NzSelectModule,
    NzInputModule,
    ReactiveFormsModule,
    NzSpinModule
  ],
  exports: [
    ListaCuentasComponent,
    NzButtonModule,
    NzTableModule,
    NzCardModule,
    DashbordRoutingModule,
    DashboardComponent
  ]
})
export class DashbordModule { }
