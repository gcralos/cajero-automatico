import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {CajeroAutomaticoService} from "../../services/cajero-automatico.service";
import {NzButtonSize} from "ng-zorro-antd/button";

@Component({
  selector: 'app-operaciones',
  templateUrl: './operaciones.component.html',
  styleUrls: ['./operaciones.component.scss']
})
export class OperacionesComponent implements OnInit {
  current = 0;
  cuentas:any=[];
  comprovante:any=[];
  index = 'First-content';
  value: any
  operacion:any;
  disables: boolean = true;
  isFormularioOperaciones: boolean= true;
  stadoOperacion: boolean = false;
  finTransacion: boolean= false;
  size: NzButtonSize = 'large';


  next(): void {
    this.current += 1;
    this.changeContent();
    this.OperacionesBancarias();
    this.isFormularioOperaciones=false;
    this.stadoOperacion=true;
    this.finTransacion=true;
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'First-content';
        break;
      }
      case 1: {
        this.index = 'Second-content';
        break;
      }
      case 2: {
        this.index = 'third-content';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }

  constructor(private cajeroAutomaticoService: CajeroAutomaticoService) {
    this.operacion = new FormGroup({
      tipoOperacion: new FormControl(),
      cuentaOrigen: new FormControl(),
      cuentaDestino: new FormControl(),
      valor: new FormControl(),
    });

  }

  ngOnInit(): void {
    this.getCuentas();
  }

  OperacionesBancarias(): void {
    let operacionValue = this.operacion.get('tipoOperacion').value;
    let numeroCuentaOrigenValue = this.operacion.get('cuentaOrigen').value;
    let numeroCuentaDestinoValue = this.operacion.get('cuentaDestino').value;
    let montoValue = this.operacion.get('valor').value;
   switch (operacionValue){
     case'DEPOSITO':
       this.setDeposito(numeroCuentaOrigenValue,montoValue);
       this.getCuentasNumerocuenta(numeroCuentaOrigenValue);
       break;
     case'RETIRO':
       this.setRetiro(numeroCuentaOrigenValue,montoValue);
       this.getCuentasNumerocuenta(numeroCuentaOrigenValue);
       break;
     case'TRANSFERENCIA':
       this.setTransferencia(numeroCuentaOrigenValue,numeroCuentaDestinoValue,montoValue);
       this.getCuentasNumerocuenta(numeroCuentaOrigenValue);
       break;
   }
  }
  getCuentas():void{
    this.cajeroAutomaticoService.listarCuentas().subscribe(result => {
      this.cuentas = result;
    });
  }
  setDeposito(numeroCuenta:string,monto:number):void{
    this.cajeroAutomaticoService.depositarSaldoCuenta(numeroCuenta,monto).subscribe(result=>{
    })
  }
  setRetiro(numeroCuenta:string,monto:number){
    this.cajeroAutomaticoService.retirarSaldoCuenta(numeroCuenta,monto).subscribe( result=>{
    })
  }
  setTransferencia(numeroCuentaOrigen:string,numeroCuenDestino:string,amonto:number){
      this.cajeroAutomaticoService.transferirCuenta(numeroCuentaOrigen,numeroCuenDestino,amonto).subscribe(result=>{

      });
  }
  getCuentasNumerocuenta(numeroCuenta:string):void{
    setTimeout(()=>{
      this.cajeroAutomaticoService.listarCuentaPorNumero(numeroCuenta).subscribe(result => {
        this.comprovante = result;
      });
    }, 1000 );
  }

  dataChanged($event: any) {
    $event==="TRANSFERENCIA"?this.disables=false: this.disables=true;
  }
}
