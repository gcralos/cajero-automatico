import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CajeroAutomaticoService {
  apiUrl: string = 'http://localhost:8080/';
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient) { }
  listarCuentas() {
    return this.http.get(`${this.apiUrl}cajero/cuentas`);
  }
  listarCuentaPorNumero(numeroCuenta:string){
    return this.http.get(`${this.apiUrl}consulta-saldo/`+numeroCuenta);
  }
   depositarSaldoCuenta(numeroCuenta: string, monto: number ): Observable<any> {
    let API_URL = `${this.apiUrl}depositar-saldo/${numeroCuenta}/${monto}`;
    return this.http.put(API_URL, { headers: this.headers }).pipe(
      catchError(this.handleError)
    )
  }
  retirarSaldoCuenta(numeroCuenta: string, monto: number ): Observable<any> {
    let API_URL = `${this.apiUrl}retirar-saldo/${numeroCuenta}/${monto}`;
    return this.http.put(API_URL, { headers: this.headers }).pipe(
      catchError(this.handleError)
    )
  }
  transferirCuenta(numeroCuentaOrigen: string,numeroCuentaDestino: string, monto: number ): Observable<any> {
    let API_URL = `${this.apiUrl}transferencia-saldo/${numeroCuentaOrigen}/${numeroCuentaDestino}/${monto}`;
    return this.http.post(API_URL, { headers: this.headers }).pipe(
      catchError(this.handleError)
    )
  }

  listarHistoricosTranferencciasCuenta(): Observable<any> {
    return this.http.get(`${this.apiUrl}transaciones-cuentas/`);
  }
  listarHistoricosTranferencciasCuentas(numeroCuenta: string): Observable<any> {
    return this.http.get(`${this.apiUrl}transaciones-cuenta/`+numeroCuenta);
  }
  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  };
}
