import { TestBed } from '@angular/core/testing';

import { CajeroAutomaticoService } from './cajero-automatico.service';

describe('CajeroAutomaticoService', () => {
  let service: CajeroAutomaticoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CajeroAutomaticoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
