# cajero-automatico
# Instrucciones  para el api

# Requisitos
Java  JDK  11 o superior

# Generacion del artefacto y lanzaminto

1)	Ingresar en la en la carpeta cajero-automatico-backend   abrir   una consola y ejecutar  el siguiente comando 
./gradlew  clean build  esto descargar y creara un  archivo  .jar
2)	Ingresamos a la siguiente ruta cajero-automatico-backend\build\libs  abrimos una cmd   y ejecutamos   java -jar cajero-automatico-0.0.1-SNAPSHOT.jar Nota: validar el nombre del archivo.

3)	En la ruta cajero-automatico-backend\src\main\resources  hay un archivo llamado cajero-automatico.postman_collection este se abre con postman  el cual tiene los puntos finales con datos de prueba 

# Instrucciones  para  la  aplicacion del web

# Requisitos
Tener instalado node  v17.8.0 y el cli  de angular en su version 13
 
1)	Ingresar en la en la carpeta cajero-automatico-backend   abrir   una consola y ejecutar  el siguiente comando npm  install esto descargar las dependencias 
2)	 En la misma ruta  despues que se  hayan descargado las  dependencias   ejecutar  ng server 
